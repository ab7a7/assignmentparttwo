﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AssignmentPartTwo
{
    public partial class showReqForm : Form
    {
        SqlCeConnection mySqlConnection;
        public showReqForm()
        {
            InitializeComponent();
            //test
            populateListBox();

            label1.Text = "Welcome " + Variables.uname + ". Your access level is: " + Variables.utype;
            if (Variables.utype == "Designer")
            {
                addRequirementToolStripMenuItem.Enabled = false;
                Delete.Enabled = false;
            }
            if (Variables.utype == "Manager")
            {
                addProjectToolStripMenuItem.Enabled = true;
            }
        }

        public void populateListBox()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=C:\temp\Database.sdf ");
            int uid = Variables.uid;
            String selcmd;
            if (Variables.utype == "Manager")
            {
                selcmd = "SELECT id, reqName, reqType, reqDesc, reqSeverity, reqMet FROM tblRequirement";
            }
            else
            {
                selcmd = "SELECT id, reqName, reqType, reqDesc, reqSeverity, reqMet FROM tblRequirement WHERE userid = '" + uid + "'";
            }

            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                lbxRequirement.Items.Clear();

                while (mySqlDataReader.Read())
                {
                    lbxRequirement.Items.Add(mySqlDataReader["Id"] + " " +
                         mySqlDataReader["reqName"] + " " + mySqlDataReader["reqType"] + " " + mySqlDataReader["reqDesc"] + " "
                         + mySqlDataReader["reqSeverity"] + " " + mySqlDataReader["reqMet"]);
                }
            }
            catch (SqlCeException ex)
            {

                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        
        private void addRequirementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addReqForm addRequirementForm = new addReqForm();

            addRequirementForm.ShowDialog();
            this.Refresh();
            populateListBox();
        }

        private void butLogout_Click(object sender, EventArgs e)
        {
            //loginform log = new loginform();
            this.Close();
        }

        private void addProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddProject addProjForm = new AddProject();
            addProjForm.Show();
            this.Refresh();
            populateListBox();
        }

        private void updateRequirementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            upReq form = new upReq();
            form.ShowDialog();
            this.Refresh();
            populateListBox();
        }
        public bool checkSelectedList()
        {
            bool rtnvalue = true;
            if (lbxRequirement.SelectedIndex == -1)
            {
                MessageBox.Show("Error: Please Select an item in List Box");
                rtnvalue = false;
            }
            return rtnvalue;
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if(checkSelectedList())
            {
                DialogResult result = MessageBox.Show("Are you sure?", "Delete", MessageBoxButtons.YesNo);
                if(result == DialogResult.Yes)
                {
                    try
                    {
                        string Id = lbxRequirement.SelectedItem.ToString();
                        string[] listItems = Id.Split(' ');
                        int reqId = int.Parse(listItems[0]);
                        //int reqId = lbxRequirement.SelectedIndex + 1;
                        String selcmd = "DELETE FROM tblRequirement WHERE Id = " + reqId;
                        SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
                        mySqlCommand.ExecuteReader();
                        populateListBox();
                    }
                    catch (SqlCeException ex)
                    {
                        MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    
                }
            }
        }
    }
}
