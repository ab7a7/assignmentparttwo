﻿namespace AssignmentPartTwo
{
    partial class showReqForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addRequirementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateRequirementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbxRequirement = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.butLogout = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(359, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addRequirementToolStripMenuItem,
            this.addProjectToolStripMenuItem,
            this.updateRequirementToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // addRequirementToolStripMenuItem
            // 
            this.addRequirementToolStripMenuItem.Name = "addRequirementToolStripMenuItem";
            this.addRequirementToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.addRequirementToolStripMenuItem.Text = "Add Requirement";
            this.addRequirementToolStripMenuItem.Click += new System.EventHandler(this.addRequirementToolStripMenuItem_Click);
            // 
            // addProjectToolStripMenuItem
            // 
            this.addProjectToolStripMenuItem.Enabled = false;
            this.addProjectToolStripMenuItem.Name = "addProjectToolStripMenuItem";
            this.addProjectToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.addProjectToolStripMenuItem.Text = "Add Project";
            this.addProjectToolStripMenuItem.Click += new System.EventHandler(this.addProjectToolStripMenuItem_Click);
            // 
            // updateRequirementToolStripMenuItem
            // 
            this.updateRequirementToolStripMenuItem.Name = "updateRequirementToolStripMenuItem";
            this.updateRequirementToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.updateRequirementToolStripMenuItem.Text = "Update Requirement";
            this.updateRequirementToolStripMenuItem.Click += new System.EventHandler(this.updateRequirementToolStripMenuItem_Click);
            // 
            // lbxRequirement
            // 
            this.lbxRequirement.FormattingEnabled = true;
            this.lbxRequirement.Location = new System.Drawing.Point(25, 68);
            this.lbxRequirement.Name = "lbxRequirement";
            this.lbxRequirement.Size = new System.Drawing.Size(334, 225);
            this.lbxRequirement.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // butLogout
            // 
            this.butLogout.Location = new System.Drawing.Point(284, 299);
            this.butLogout.Name = "butLogout";
            this.butLogout.Size = new System.Drawing.Size(75, 23);
            this.butLogout.TabIndex = 3;
            this.butLogout.Text = "Logout";
            this.butLogout.UseVisualStyleBackColor = true;
            this.butLogout.Click += new System.EventHandler(this.butLogout_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(25, 299);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(75, 23);
            this.Delete.TabIndex = 4;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // showReqForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 327);
            this.ControlBox = false;
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.butLogout);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbxRequirement);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "showReqForm";
            this.Text = "Requirements";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addRequirementToolStripMenuItem;
        private System.Windows.Forms.ListBox lbxRequirement;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butLogout;
        private System.Windows.Forms.ToolStripMenuItem addProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateRequirementToolStripMenuItem;
        private System.Windows.Forms.Button Delete;
    }
}

