﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AssignmentPartTwo
{
    public partial class AddProject : Form
    {
        SqlCeConnection mySqlConnection;
        public AddProject()
        {
            InitializeComponent();
            mySqlConnection =
                new SqlCeConnection(@"Data Source=C:\temp\Database.sdf ");
            mySqlConnection.Open();
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtProjName.Text) ||
                string.IsNullOrEmpty(txtProjType.Text) )
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }
            return (rtnvalue);
        }

        public void insertRecord(String name, String type, String commandString)
        {
            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@name", name);
                cmdInsert.Parameters.AddWithValue("@type", type);

                cmdInsert.ExecuteNonQuery();
                MessageBox.Show("Insert Success");
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String commandString = "INSERT INTO tblProject(name, type) VALUES (@name, @type)";

            insertRecord(txtProjName.Text, txtProjType.Text, commandString);
            this.Close();
        }
    }
}
