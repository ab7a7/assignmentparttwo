﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AssignmentPartTwo
{
    public partial class upReq : Form
    {
        SqlCeConnection mySqlConnection;
        public upReq()
        {
            InitializeComponent();
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=C:\temp\Database.sdf ");
            mySqlConnection.Open();
        }

        public bool checkRequirement()
        {
            bool rtnvalue = true;
            String selcmd = "SELECT COUNT(*) FROM tblRequirement";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            int countRow = int.Parse(mySqlCommand.ExecuteScalar().ToString());
            if (numReqId.Value <= 0 || numReqId.Value > countRow)
            {
                MessageBox.Show("Error: Requirement does not exist.");
                rtnvalue = false;
            }

            return (rtnvalue);

        }
        private void button2_Click(object sender, EventArgs e)
        {
            if(checkRequirement())
            {
                txtDesc.Enabled = true;
                yesRadButton.Enabled = true;
                noRadButton.Enabled = true;
                dateMet.Enabled = true;
                upButton.Enabled = true;
            }
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtDesc.Text) ||
                (!yesRadButton.Checked && !noRadButton.Checked))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }
            if (yesRadButton.Checked)
            {
                if (dateMet.Value < DateTime.Today)
                {
                    MessageBox.Show("Error: Date Met must be higher than today.");
                    rtnvalue = false;
                }
            }
            return (rtnvalue);

        }

        public void insertRecord(String desc, bool requirementMet, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@desc", desc);
                cmdInsert.Parameters.AddWithValue("@reqMet", requirementMet);
                if (yesRadButton.Checked)
                {
                    commandString = commandString.Replace("@reqMet", "@reqMet, reqDateMet = @datemet");
                    cmdInsert.Parameters.AddWithValue("@datemet", dateMet.Value);
                }
                cmdInsert.ExecuteNonQuery();
                MessageBox.Show("Update Success");
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                String commandString = "UPDATE tblRequirement SET reqDesc = @desc, reqMet = @reqMet WHERE tblrequirement.Id = '" + numReqId.Value + "'";
                bool requirementMet = true;
                if (!yesRadButton.Checked)
                {
                    requirementMet = false;
                }
                insertRecord(txtDesc.Text, requirementMet, commandString);
                this.Close();
            }
        }
    }
}
