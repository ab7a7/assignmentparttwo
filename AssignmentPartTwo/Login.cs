﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AssignmentPartTwo
{
    public partial class loginform : Form
    {
        //public int a = 5;
        SqlCeConnection mySqlConnection;
        int count = 0;
        public loginform()
        {
            InitializeComponent();
            mySqlConnection =
                   new SqlCeConnection(@"Data Source=C:\temp\Database.sdf ");
            mySqlConnection.Open();
            comboReg.Items.Add("Developer");
            comboReg.Items.Add("Manager");
            comboReg.Items.Add("Designer");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUser.Text) || string.IsNullOrEmpty(txtPass.Text))
            {
                MessageBox.Show("Please Enter Username and Password.");
            }
            else
            {
                String selcmd = "SELECT Id, username, usertype FROM tbluser WHERE username = '" + txtUser.Text + "' AND password = '" + txtPass.Text + "'";
                SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
                try
                {
                    SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                    count = 0;
                    while (mySqlDataReader.Read())
                    {
                        count += 1;
                        Variables.uname = mySqlDataReader["username"].ToString();
                        Variables.uid = int.Parse(mySqlDataReader["Id"].ToString());
                        Variables.utype = mySqlDataReader["usertype"].ToString();
                    }

                    if (count == 0)
                    {
                        MessageBox.Show("Please Enter Correct Username and Password.");
                    }
                    else if (count == 1)
                    {
                        showReqForm reqForm = new showReqForm();
                        reqForm.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Duplicate users.");
                    }   
                }
                catch (SqlCeException ex)
                {

                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtUserReg.Text) || string.IsNullOrEmpty(txtPassReg.Text) || string.IsNullOrEmpty(comboReg.Text))
            {
                MessageBox.Show("Please Enter details.");
            }
            else
            {
                String commandString = "INSERT INTO tbluser(username, password, usertype) VALUES (@username, @password, @usertype)";
                insertRecord(txtUserReg.Text, txtPassReg.Text, comboReg.Text, commandString);
            }
        }

        public void insertRecord(String uname, String pass, String type, String command)
        {
            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(command, mySqlConnection);
                cmdInsert.Parameters.AddWithValue("@username", uname);
                cmdInsert.Parameters.AddWithValue("@password", pass);
                cmdInsert.Parameters.AddWithValue("@usertype", type);
                cmdInsert.ExecuteNonQuery();
                MessageBox.Show("Successful Registeration, Please Logiin to continue");
                tabControl1.SelectedIndex = 0;
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
