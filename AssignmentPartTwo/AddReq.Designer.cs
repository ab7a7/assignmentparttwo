﻿namespace AssignmentPartTwo
{
    partial class addReqForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.txtSev = new System.Windows.Forms.TextBox();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.dateMet = new System.Windows.Forms.DateTimePicker();
            this.dateAdded = new System.Windows.Forms.DateTimePicker();
            this.dateRequired = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.noRadButton = new System.Windows.Forms.RadioButton();
            this.yesRadButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.numProjId = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numProjId)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(150, 360);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Requirement Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Requirement Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Requirement Description";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Requirement Severity";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 304);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Requirement Date Met";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(182, 57);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Requirement Date Added";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 267);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Requirement Date Required";
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(182, 96);
            this.txtType.MaxLength = 50;
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(100, 20);
            this.txtType.TabIndex = 11;
            // 
            // txtSev
            // 
            this.txtSev.Location = new System.Drawing.Point(182, 171);
            this.txtSev.MaxLength = 50;
            this.txtSev.Name = "txtSev";
            this.txtSev.Size = new System.Drawing.Size(100, 20);
            this.txtSev.TabIndex = 13;
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(182, 136);
            this.txtDesc.MaxLength = 100;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(100, 20);
            this.txtDesc.TabIndex = 12;
            // 
            // dateMet
            // 
            this.dateMet.Location = new System.Drawing.Point(182, 304);
            this.dateMet.Name = "dateMet";
            this.dateMet.Size = new System.Drawing.Size(200, 20);
            this.dateMet.TabIndex = 14;
            // 
            // dateAdded
            // 
            this.dateAdded.Location = new System.Drawing.Point(182, 235);
            this.dateAdded.Name = "dateAdded";
            this.dateAdded.Size = new System.Drawing.Size(200, 20);
            this.dateAdded.TabIndex = 15;
            // 
            // dateRequired
            // 
            this.dateRequired.Location = new System.Drawing.Point(182, 267);
            this.dateRequired.Name = "dateRequired";
            this.dateRequired.Size = new System.Drawing.Size(200, 20);
            this.dateRequired.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 203);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Requirement Met";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.noRadButton);
            this.groupBox1.Controls.Add(this.yesRadButton);
            this.groupBox1.Location = new System.Drawing.Point(182, 197);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 32);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            // 
            // noRadButton
            // 
            this.noRadButton.AutoSize = true;
            this.noRadButton.Location = new System.Drawing.Point(119, 9);
            this.noRadButton.Name = "noRadButton";
            this.noRadButton.Size = new System.Drawing.Size(39, 17);
            this.noRadButton.TabIndex = 1;
            this.noRadButton.TabStop = true;
            this.noRadButton.Text = "No";
            this.noRadButton.UseVisualStyleBackColor = true;
            // 
            // yesRadButton
            // 
            this.yesRadButton.AutoSize = true;
            this.yesRadButton.Location = new System.Drawing.Point(0, 9);
            this.yesRadButton.Name = "yesRadButton";
            this.yesRadButton.Size = new System.Drawing.Size(43, 17);
            this.yesRadButton.TabIndex = 0;
            this.yesRadButton.TabStop = true;
            this.yesRadButton.Text = "Yes";
            this.yesRadButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Project ID";
            // 
            // numProjId
            // 
            this.numProjId.Location = new System.Drawing.Point(182, 26);
            this.numProjId.Name = "numProjId";
            this.numProjId.Size = new System.Drawing.Size(120, 20);
            this.numProjId.TabIndex = 23;
            this.numProjId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // addReqForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 395);
            this.Controls.Add(this.numProjId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dateRequired);
            this.Controls.Add(this.dateAdded);
            this.Controls.Add(this.dateMet);
            this.Controls.Add(this.txtSev);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.txtType);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Name = "addReqForm";
            this.Text = "Insert Requirement";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numProjId)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.TextBox txtSev;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.DateTimePicker dateMet;
        private System.Windows.Forms.DateTimePicker dateAdded;
        private System.Windows.Forms.DateTimePicker dateRequired;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton noRadButton;
        private System.Windows.Forms.RadioButton yesRadButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numProjId;
    }
}