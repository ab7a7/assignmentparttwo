﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AssignmentPartTwo
{
    public partial class addReqForm : Form
    {
        SqlCeConnection mySqlConnection;
        public addReqForm()
        {
            InitializeComponent();
            mySqlConnection =
                new SqlCeConnection(@"Data Source=C:\temp\Database.sdf ");
            mySqlConnection.Open();

        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtName.Text) ||
                string.IsNullOrEmpty(txtType.Text) ||
                string.IsNullOrEmpty(txtDesc.Text) ||
                string.IsNullOrEmpty(txtSev.Text) ||
                (!yesRadButton.Checked && !noRadButton.Checked))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }
            if (dateAdded.Value >= dateRequired.Value)
            {
                MessageBox.Show("Error: Date required must be more than date added.");
                rtnvalue = false;
            }
            if (yesRadButton.Checked)
            {
                if(dateMet.Value >= dateRequired.Value || dateMet.Value <= dateAdded.Value)
                {
                    MessageBox.Show("Error: Date Met must be between date added and date required.");
                    rtnvalue = false;
                }
            }
            return (rtnvalue);

        }

        public void insertRecord(int userid, int projid, String name, String type, String desc, String severity, bool requirementMet, DateTime dateAdded, DateTime dateRequired, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@userid", userid);
                cmdInsert.Parameters.AddWithValue("@projectid", projid);
                cmdInsert.Parameters.AddWithValue("@name", name);
                cmdInsert.Parameters.AddWithValue("@type", type);
                cmdInsert.Parameters.AddWithValue("@desc", desc);
                cmdInsert.Parameters.AddWithValue("@severity", severity);
                cmdInsert.Parameters.AddWithValue("@reqMet", requirementMet);
                cmdInsert.Parameters.AddWithValue("@dateadded", dateAdded);
                cmdInsert.Parameters.AddWithValue("@daterequired", dateRequired);
                if(yesRadButton.Checked)
                {
                    commandString = commandString.Replace("reqDateRequired)","reqDateRequired, reqDateMet)");
                    commandString = commandString.Replace("@daterequired)","@daterequired, @datemet)");
                    cmdInsert.Parameters.AddWithValue("@datemet", dateMet.Value);
                }
                cmdInsert.ExecuteNonQuery();
                MessageBox.Show("Insert Success");
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool checkProject()
        {
            bool rtnvalue = true;
            String selcmd = "SELECT COUNT(*) FROM tblProject";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            int countRow = int.Parse(mySqlCommand.ExecuteScalar().ToString());
            if (numProjId.Value < 0 || numProjId.Value > countRow)
            {
                MessageBox.Show("Error: Project does not exist.");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkInputs() && checkProject())
            {
                int userid = Variables.uid;
                String commandString = "INSERT INTO tblRequirement(userid, projectid, reqName, reqType, reqDesc, reqSeverity, reqMet, reqDateAdded, reqDateRequired) VALUES (@userid, @projectid, @name, @type, @desc, @severity, @reqMet, @dateadded, @daterequired)";
                bool requirementMet = true;
                if (!yesRadButton.Checked)
                {
                    requirementMet = false;
                }
                insertRecord(userid, int.Parse(numProjId.Value.ToString()), txtName.Text, txtType.Text, txtDesc.Text, txtSev.Text, requirementMet, dateAdded.Value.Date, dateRequired.Value.Date, commandString);
                this.Close();
            }

        }
    }
}
