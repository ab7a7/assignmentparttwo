﻿namespace AssignmentPartTwo
{
    partial class upReq
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.noRadButton = new System.Windows.Forms.RadioButton();
            this.yesRadButton = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateMet = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.upButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.searchButton = new System.Windows.Forms.Button();
            this.numReqId = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numReqId)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.noRadButton);
            this.groupBox1.Controls.Add(this.yesRadButton);
            this.groupBox1.Location = new System.Drawing.Point(168, 133);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 32);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            // 
            // noRadButton
            // 
            this.noRadButton.AutoSize = true;
            this.noRadButton.Enabled = false;
            this.noRadButton.Location = new System.Drawing.Point(119, 9);
            this.noRadButton.Name = "noRadButton";
            this.noRadButton.Size = new System.Drawing.Size(39, 17);
            this.noRadButton.TabIndex = 1;
            this.noRadButton.TabStop = true;
            this.noRadButton.Text = "No";
            this.noRadButton.UseVisualStyleBackColor = true;
            // 
            // yesRadButton
            // 
            this.yesRadButton.AutoSize = true;
            this.yesRadButton.Enabled = false;
            this.yesRadButton.Location = new System.Drawing.Point(0, 9);
            this.yesRadButton.Name = "yesRadButton";
            this.yesRadButton.Size = new System.Drawing.Size(43, 17);
            this.yesRadButton.TabIndex = 0;
            this.yesRadButton.TabStop = true;
            this.yesRadButton.Text = "Yes";
            this.yesRadButton.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "Requirement Met";
            // 
            // txtDesc
            // 
            this.txtDesc.Enabled = false;
            this.txtDesc.Location = new System.Drawing.Point(168, 90);
            this.txtDesc.MaxLength = 100;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(100, 20);
            this.txtDesc.TabIndex = 33;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Requirement Description";
            // 
            // dateMet
            // 
            this.dateMet.Enabled = false;
            this.dateMet.Location = new System.Drawing.Point(162, 179);
            this.dateMet.Name = "dateMet";
            this.dateMet.Size = new System.Drawing.Size(200, 20);
            this.dateMet.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Requirement Date Met";
            // 
            // upButton
            // 
            this.upButton.Enabled = false;
            this.upButton.Location = new System.Drawing.Point(122, 232);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(75, 23);
            this.upButton.TabIndex = 40;
            this.upButton.Text = "Update";
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Requirement ID";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(287, 31);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 43;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // numReqId
            // 
            this.numReqId.Location = new System.Drawing.Point(122, 34);
            this.numReqId.Name = "numReqId";
            this.numReqId.Size = new System.Drawing.Size(120, 20);
            this.numReqId.TabIndex = 44;
            // 
            // upReq
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 327);
            this.Controls.Add(this.numReqId);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.upButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dateMet);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Name = "upReq";
            this.Text = "upReq";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numReqId)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton noRadButton;
        private System.Windows.Forms.RadioButton yesRadButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateMet;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.NumericUpDown numReqId;
    }
}